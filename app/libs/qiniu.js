const qiniu = require('qiniu');
const formidable = require('formidable')
const path = require('path')
const _ = require('lodash');
const qiniuconfig = require('../config/qiniu.js');
const accessKey = qiniuconfig.accessKey;
const secretKey = qiniuconfig.secretKey;
const publicBucketDomain = qiniuconfig.publicBucketDomain;
const mac = new qiniu.auth.digest.Mac(accessKey, secretKey);
const config = new qiniu.conf.Config();
const fs = require('fs')

let cacheImageJson;

const bucket = qiniuconfig.bucket;
let nextMarker; // 七牛下一页hash
// @param options 列举操作的可选参数
// prefix    列举的文件前缀
// marker    上一次列举返回的位置标记，作为本次列举的起点信息
// limit     每次返回的最大列举文件数量
// delimiter 指定目录分隔符

let getAllImages = function() {
    if (cacheImageJson && !_.isEmpty(cacheImageJson)) {
      return cacheImageJson;
    } else {
      const bucketManager = new qiniu.rs.BucketManager(mac, config);
      const options = {
        limit: 1000,
        prefix: ''
      };

      if (nextMarker) {
        options.marker = nextMarker;
      }

      return new Promise(function (resolve, reject) {
        bucketManager.listPrefix(bucket, options, function (err, respBody, respInfo) {
          if (err) {
            throw err;
          }

          if (respInfo.statusCode === 200) {
            // 如果这个nextMarker不为空，那么还有未列举完毕的文件列表，下次调用listPrefix的时候
            // 指定options里面的marker为这个值
            nextMarker = respBody.marker;
            // const commonPrefixes = respBody.commonPrefixes;
            // console.log(nextMarker);
            const items = respBody.items;

            const results = {
              status: respInfo.statusCode,
              host: publicBucketDomain,
              count: items.length,
              data: items
            };
            cacheImageJson = _.cloneDeep(results);

            resolve(results);
          }
        });
      });
    }
};

let uploadFileToQiNiu = function (req, callback) {
  var formUploader = new qiniu.form_up.FormUploader(config);
  let form = new formidable.IncomingForm()
  let fileDir = path.join(__dirname, '../../', 'public/resourse');
  fsExists(fileDir, function (){
    form.uploadDir = _.cloneDeep(fileDir)
    form.keepExtensions = true;
    form.parse(req, function (err, fields, files) {
      if (err) {
        console.log(err);
      }
      // 上传的文件信息
      let FileInfo = files.file
      let key = fields.photo_name ? fields.photo_name : FileInfo.name;
      let ran = parseInt(Math.random() * 8999 + 10000);

      // 配置七牛参数
      let options = {
        scope: bucket,
        returnBody: '{"name":"$(key)","hash":"$(etag)","fsize":$(fsize),"w": $(imageInfo.width), "h": $(imageInfo.height)}',
        callbackBodyType: 'application/json'
      };
      let putPolicy = new qiniu.rs.PutPolicy(options)
      let token = putPolicy.uploadToken(mac)
      let extra = new qiniu.form_up.PutExtra();

      // 初始化数据
      let result_fields = {
        create_time: String(new Date().getTime()),
        qiniu: null,
        path: "",
        name: key,
        format: FileInfo.type,
        size: String(FileInfo.size),
        hash: "",
        w: "",
        h: '',
        album: fields
      }

      // 修改本地文件存储路径
      // 旧的文件路径 FileInfo.path
      let oldpath = FileInfo.path;
      // 新的文件路径
      let newpath = "/ran_" + ran + "_" + key;
      //要上传文件的本地路径
      let filePath = fileDir + newpath

      console.log("===========================")
      fs.rename(oldpath, filePath, function (err) {
        if (err) {
          throw Error("改名失败");
        }
        // 上传到七牛云存储
        if (FileInfo.size < 509715200) {

          // 上传成功之后判断类型是否是视频, 对视频进行转码
          let videoTypes = ["MOV", "mov", "video/quicktime"];
          console.log(result_fields.format);
          if (videoTypes.filter(function (item) { return result_fields.format.match(item) }).length) {
            let fops = "avthumb/mp4/ab/160k/ar/44100/acodec/libfaac/r/30/vb/2200k/vcodec/libx264/s/1280x720/autoscale/1/stripmeta/0";
            let saveas_key = qiniu.util.urlsafeBase64Encode(`bucket:${key}`);
            fops = fops + '|saveas/' + saveas_key;

            //上传策略中设置pipeline以及fops
            function uptoken(mac) {
              putPolicy.persistentOps = fops;
              putPolicy.persistentPipeline = "qiniu";
              return putPolicy.uploadToken(mac);
            }

            //生成上传 Token
            token = uptoken(mac);

            //构造上传函数
            console.log(token)
            uploadFileParseRet(token, filePath, extra)
            // function uploadFile(uptoken, key, localFile) {
            //   formUploader.putFile(uptoken, "", localFile, extra, function (err, ret) {
            //     parseRet(err, ret)
            //   });
            // }

            // //调用uploadFile上传
            // uploadFile(token, key, filePath);
          } else { // 上传七牛后返回的文件信息
            uploadFileParseRet(token, filePath, extra)
          }

          function uploadFileParseRet(token, filePath, extra){
            formUploader.putFile(token, "", filePath, extra, function (err, ret) {
              if (err) {
                console.log(err)
              }
              result_fields.path = "/resourse" + newpath;
              result_fields.w = String(ret.w);
              result_fields.h = String(ret.h);
              result_fields.hash = ret.hash;
              callback(result_fields)
            })
          }
        } else {
          callback(result_fields)
        }
      });
    })
  })
}

function fsExists(dirPath, callback){
  fs.exists(dirPath, function (exist) {
    if (!exist) {
      fs.mkdir(dirPath)
    }
    callback()
  })  
}

// 获取上传的文件信息并进行解析
function getFileInfo(chunks, size) {
  var buffer = Buffer.concat(chunks, size);
  if (!size) {
    res.writeHead(404);
    res.end('');
    return;
  }

  var rems = [];

  //根据\r\n分离数据和报头
  for (var i = 0; i < buffer.length; i++) {
    var v = buffer[i];
    var v2 = buffer[i + 1];
    if (v == 13 && v2 == 10) {
      rems.push(i);
    }
  }

  //文件信息
  var picmsg_1 = buffer.slice(rems[0] + 2, rems[1]).toString();
  var filename = picmsg_1.match(/filename=".*"/g)[0].split('"')[1];

  //文件数据
  var nbuf = buffer.slice(rems[3] + 2, rems[rems.length - 2]);
  return {
    filename: filename,
    size: size,
    buffer: nbuf
  }
}

module.exports = {
  getqiniuimages: getAllImages,
  getFileInfo: getFileInfo,
  uploadFileToQiNiu: uploadFileToQiNiu
}