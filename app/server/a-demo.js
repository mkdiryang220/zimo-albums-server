const name = "album"
import MysqlUtil from '../common/mysql.util'

module.exports = function (app) {
  MysqlUtil.createUtilRouters(app, name)
}