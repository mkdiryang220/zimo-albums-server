import mysql_util from '../common/mysql.util.js'
import uuidV1 from 'uuid/v1'

module.exports = function (app) {
  // 登陆
  app.post('/api/login', function (req, res, next) {
    // console.log("/login", req.headers)
    let u = req.body.u;
    let loginInfo = u.split('_')  
    let loginArg = {
      username: loginInfo[0],
      password: loginInfo[2]
    }
    let time = new Date().getTime();
    if (loginInfo.length !== 4 && time - loginInfo[3] < 1000){
      res.json({status: 0})
    }
    mysql_util.useDBAsApi("user").getRowByWhere(loginArg).then(function (resu) {
      let result = {
        status: resu.status,
        data: `${uuidV1()}_${resu.result[0].userid}_${new Date().getTime()}`
      }
      res.json(result)
    })

  })
}