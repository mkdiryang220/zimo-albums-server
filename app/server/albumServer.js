import mysql_util from '../common/mysql.util.js';
import _ from 'lodash';

module.exports = function (app) {
  // 获取用户所有相册
  app.post('/api/getalbums', function (req, res, next) {
    mysql_util.useDBAsApi("album").getRowByWhere({ founder_id: req.body.uid, is_private: "0" }, "or").then(function (data) {
      res.json(data)
    })
  })

  // 获取用户所有照片
  app.post('/api/getallphotobyuser', async function (req, res, next) {
    let allAlbumByUser = await mysql_util.useDBAsApi("album").getRowByWhere({ founder_id: req.body.uid, is_private: "0"}, "or");
    if (!allAlbumByUser.status) { res.json({ state: 0 }) }
    let album_ids = []
    allAlbumByUser.result.forEach(function (album) {
      album_ids.push(album.album_id)
    })
    
    let allAlbumPhoto = await mysql_util.useDBAsApi("album_photo").getRowByIds(album_ids, "album_id");
    if (!allAlbumPhoto.status) { res.json({ state: 0 }) }
    let photo_ids = []
    allAlbumPhoto.result.forEach(function (photo) {
      photo_ids.push(photo.photo_id)
    })
    

    let allPhotos = await mysql_util.useDBAsApi("photo").getRowByIds(photo_ids);
    res.json(allPhotos)
  })

  // 添加相册
  app.post('/api/addAlbum', function (req, res, next) {
    let fields = {
      founder_id: req.body.i,
      is_private: req.body.is_private === "private"?1:0,
      album_name: req.body.name,
      album_home_link: req.body.homelink,
      remark: req.body.desc,
      create_time: String(new Date().getTime())
    }
    mysql_util.useDBAsApi("album").addRow(fields).then(function (data) {
      fields.album_id = data.result[0];
      data.result = _.cloneDeep(fields);
      res.json(data);
    })
  })

  // 获取相册内所有照片信息
  app.post('/api/getalbumphotos', function (req, res, next){
    if (!req.body.id || req.body.id === "0"){
      gettestalbum(req, res, next)
    }else{
      getalbumphotos(req, res, next)
    }
  })
  function getalbumphotos(req, res, next) {
    // 先到相册关联表查询关联信息
    mysql_util.useDBAsApi("album_photo").getRowByWhere({ album_id: req.body.id }).then(function (data) {

      if (data.result) {
        let photoIds = [];
        data.result.forEach(item => {
          photoIds.push(item.photo_id)
        });
        // 得到所有相关联的 ids, 查询 ids 得到相关照片
        if (photoIds.length) {
          mysql_util.useDBAsApi("photo").getRowByIds(photoIds).then(function (data) {
            res.json(data)
          })
        } else {
          res.json({
            status: 0,
            msg: "没有找到相册相关照片, 请先上传照片"
          })
        }
      } else {
        res.json({
          status: 0,
          msg: "没有找到相册相关照片, 请先上传照片"
        })
      }
    })
  }

  // 获取测试相册
  app.post('/api/gettestalbum', gettestalbum)
  function gettestalbum(req, res, next) {
    mysql_util.useDBAsApi("album_photo").getAllRows().then(function (data) {
      if (data.result) {
        let photoIds = [];
        data.result.forEach(item => {
          if (item.album_id === 'undefined' || item.album_id.length !== 36) {
            photoIds.push(item.photo_id)
          }
        });
        if (photoIds.length) {
          mysql_util.useDBAsApi("photo").getRowByIds(photoIds).then(function (data) {
            res.json(data)
          })
        } else {
          res.json({
            status: 0,
            msg: "没有找到相册相关照片, 请先上传照片"
          })
        }
      }
    })
  }

  // 删除相册
  app.post('/api/deletealbum', function (req, res, next) {

    mysql_util.useDBAsApi("album_photo").getRowByWhere({ album_id: req.body.id }).then(function (data) {

      if (data.result) {
        res.json({
          status: 0,
          msg: "请先删除照片后再删除相册"
        })
      } else {
        mysql_util.useDBAsApi("album").deleteRow(req.body.id).then(function (data) {
          res.json(data)
        })
      }
    })
  })
}