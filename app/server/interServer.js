import mysql_util from '../common/mysql.util.js'
// import platform from 'platform';

function parseText(header, text) {
  let keys = Object.keys(header);
  let values = Object.values(header);
  let strK = keys.join(',');
  let strV = values.join(',');
  let result = strK + text + strV
  if (result === text) {
    result = ""
  }
  return result
}

module.exports = function (app) {

  app.use("*", function (req, res, next) {

    let usertoken = req.headers.usertoken;
    let authorization = req.headers.authorization
    delete req.headers.usertoken;
    delete req.headers.authorization;
    
    let params = {
      require_host: req.headers.host,
      require_user_agent: req.headers["user-agent"],
      require_time: req._startTime ? new Date(req._startTime).getTime(): "",
      require_api: req.originalUrl || req.baseUrl,
      require_params: parseText(req.body, 'require_params'),
      require_cookies: parseText(req.cookies, 'require_cookies'),
      require_headers: parseText(req.headers, 'require_headers'),
    }

    mysql_util.useDBAsApi("intercepting").addRow(params)

    // 不需要验证的接口
    let ApiPass = ['/api/login', '/api/gettestalbum']
    if (!ApiPass.includes(req.originalUrl)){
      // 验证权限
      // let author = checkAuthorization(authorization)
      let author1 = checkUserToken(usertoken)
      if (!author1) {
        res.json({ status: 0, result: "未授权" })
      } else {
        next()
      }
    }else{
      next()
    }

    // 验证 authorization
    function checkAuthorization(authorization){
      if (authorization) {
        if (authorization.length === 7 && authorization.lastIndexOf(1) === 6) {
          return true
        }
      } else {
        return false
      }
    }
    // 验证 usertoken
    function checkUserToken(usertokenStr){
      let usertoken = JSON.parse(usertokenStr);
      let nowDate = new Date().getTime();
      req.headers.userid = usertoken.i;
      // 验证时间
      if (!usertoken || nowDate - Number(usertoken.d) > 60 * 60 * 1000){
        return false
      }
      return true
    }
  })

  app.get("/api/recordlog", function (req, res, next) {
    mysql_util.useDBAsApi("intercepting").getAllRows().then(function(data){
      res.json(data)
    }).catch(function (err) {
      next()
    })
  })
}