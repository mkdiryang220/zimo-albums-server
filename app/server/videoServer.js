import qiniu from '../libs/qiniu.js'
import mysql_util from '../common/mysql.util.js'

module.exports = function (app) {
  // 获取用户所有视频
  app.post('/api/getvideos', function (req, res, next) {
    mysql_util.useDBAsApi("video").getRowById(req.headers.userid, "userid").then(function (data) {
      res.json(data)
    })
  })

  // 上传图片
  app.post('/api/uploadvideo', function (req, res, next) {
    qiniu.uploadFileToQiNiu(req, function (data) {
      let result = {
        size: data.size,
        format: data.format,
        userid: req.headers.userid,
        link: data.path,
        name: data.name,
        qiniu_link: data.hash,
        create_time: data.create_time
      }
      // 保存照片到数据库
      mysql_util.useDBAsApi("video").addRow(result).then(function (data_photo) {
        // 返回数据
        if (data_photo.status) {
          res.json({ status: 1, result: result });
        } else {
          res.json(data_photo);
        }
      })
    });
  })

  // 删除一张照片和照片的关联表
  app.post('/api/deletevideo', async function (req, res, next) {
    let photoInfo = await mysql_util.useDBAsApi("video").deleteRow(req.body.id);
    if (photoInfo.status && photoAlbumConcat.status) {
      res.json({ status: 1, msg: '删除视频成功' })
    } else {
      res.json({ status: 0, msg: photoInfo.msg + ',' + photoAlbumConcat.msg })
    }
  })
}