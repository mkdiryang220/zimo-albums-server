import qiniu from '../libs/qiniu.js'
import mysql_util from '../common/mysql.util.js'

module.exports = function (app) {
  // 上传图片
  app.post('/api/upload', function (req, res, next) {
    qiniu.uploadFileToQiNiu(req, function (data) {
      let result = {
        photo_name: data.name, // 文件名称
        photo_size: data.size,
        photo_format: data.format,
        photo_link: data.path,  // 本地路径
        photo_width: data.w,
        photo_height: data.h,
        photo_qiniu_link: data.hash,
        create_time: data.create_time
      }
      // 保存照片到数据库
      mysql_util.useDBAsApi("photo").addRow(result).then(function (data_photo) {
        // 保存关联数据到数据库
        let album_photo_result = {
          photo_id: data_photo.result[0],
          album_id: data.album.album_id,
          remark: ""
        }
        mysql_util.useDBAsApi("album_photo").addRow(album_photo_result);
        // 返回数据
        if (data_photo.status){
          res.json({ status: 1, result: result});
        }else{
          res.json(data_photo);
        }
      })
    });
  })
  
  // 删除一张照片和照片的关联表
  app.post('/api/deletephoto', async function (req, res, next) {
    let photoInfo = await mysql_util.useDBAsApi("photo").deleteRow(req.body.id) ;
    let photoAlbumConcat = await mysql_util.useDBAsApi("album_photo").deleteRow(req.body.id, "photo_id");
    if (photoInfo.status && photoAlbumConcat.status){
      res.json({status: 1, msg: '删除照片和关联信息成功'})
    } else {
      res.json({ status: 0, msg: photoInfo.msg + ',' + photoAlbumConcat.msg })
    }
  })
}