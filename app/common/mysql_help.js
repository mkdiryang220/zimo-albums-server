let mysql = require('mysql');
let _ = require('lodash');
let msconfig = require('../config/config').mysql;
let logger = require('./util/logger')();
let dbs = ['notification', 'job'];
let pools = {};
let dbConstructs = require('../config/dbConstructs');

dbs.forEach(function (db) {
    pools[db] = mysql.createPool({
        host: msconfig.host,
        user: msconfig.user,
        password: msconfig.password,
        database: db
    })
});

let titleText = {
    area_info: "区域保卫-区域"
};

function mysql_help(dbName, tableName) {
    let searchDB = sqlOption(dbName);
    let table_name = tableName;
    let dbConstruct = dbConstructs[tableName];
    let row_id_name = Object.keys(dbConstruct)[0];
    let userText = getTextTip(titleText[tableName]);

    if (!table_name) {
        console.error("缺少表名");
        return
    } else if (!row_id_name) {
        console.error("缺少行 id");
        return
    } else if (!userText) {
        console.error("缺少提示字符");
        return
    }
    return {
        getAllRows: function () {
            return searchDB.selectAll(table_name)
        },
        getRowById: function (id) {   // 查询一个id, id为字符串
            let where = `${row_id_name}='${id}'`;
            return searchDB.selectByWhere(table_name, where, userText.find)
        },
        getRowByIds: function (ids, rowName) { // 查询多个ids或者其他字段, name:字段名称(如果字段名称不存在, 则直接按id查询数据)
            let field_name = rowName ? rowName : row_id_name;
            let idsField = [];
            ids.forEach(id => {
                idsField.push(`'${id}'`)
            });
            let where = `${field_name} in (${idsField.join(',')})`;
            return searchDB.selectByWhere(table_name, where, userText.find)
        },
        getRowByWhere: function (field, orAnd) {
            let where = whereField(field, orAnd);
            return searchDB.selectByWhere(table_name, where, userText.find)
        },
        addRow: function (rowData) {// 加入整条数据
            let arg = sortArg(rowData);
            return searchDB.insert(table_name, arg, userText.create);
        },
        updataRow: function (rowData,rowName) {// rowData:需要更新的数据, rowName:字段名称(如果字段名称不存在, 则直接按id更新数据)
            let delByRowName = rowName?rowName:row_id_name;
            let arg = {
                keys: assignSqlArg(rowData),
                where: `${delByRowName}='${rowData[delByRowName]}'`
            };
            return searchDB.updateById(table_name, arg, userText.update)
        },
        deleteRow: function (field,rowName) {// field: 值，rowName:字段名称(如果字段名称不存在, 则直接按id删除数据)
            if(!rowName){rowName = row_id_name}
            let where = `${rowName} in('${field}')`;
            return searchDB.delete(table_name, where, userText.delete)
        },
        deleteRows: function (rowName, rows) {
            let where = `${rowName} in('${ArrChangeToSqlStr(rows)}')`;
            return searchDB.delete(table_name, where, userText.delete)
        }
    }
}

function getTextTip(text) {
    return {
        create: "添加 " + text,
        update: "更新 " + text,
        delete: "删除 " + text,
        find: "查询 " + text
    }
}

// 公共方法
function assignSqlArg(rowData){
    let result = [];
    Object.keys(rowData).forEach((key) => {
        if (rowData[key]){
            result.push(`${key}='${rowData[key]}'`)
        }
    });
    return result
}

// 数组转换为sql字符串
function ArrChangeToSqlStr(Arr){
    let result = [];
    Arr.forEach(function(str){
        result.push(`'${str}'`)
    });
    return result
}

// 调整参数顺序
function sortArg(rowData){
    let keys = _.keys(rowData);
    let values = [];
    keys.forEach(function(key){
        values.push(`'${rowData[key]}'`);
    });
    return {
        keys: keys.join(","),
        values: values.join(",")
    }
}

// 转换对象为where条件语句
function whereField(field, orAnd){
    let condition = orAnd==='or' ? ' or ' : ' and ';
    let keys = _.keys(field);
    let result = [];
    keys.forEach(function(key){
        result.push(`${key}='${field[key]}'`);
    });
    return result.join(condition)
}

function sqlOption(db) {
    let pool = pools[db];
    let getConnetion = function (sql, cb) {
        pool.getConnection(function (err, connection) {
            if (err) {
                logger.error(err);
                throw err
            }
            connection.query(sql, function (error, results, fields) {
                if (error) {
                    logger.error(error);
                }
                cb(error, results, fields);
                connection.release();
            })
        })
    };
    return {
        insert: function (tableName, args, text) {
            let sql = `INSERT INTO ${tableName} (${args.keys}) VALUES (${args.values})`;
            console.log('-----------------------');
            console.log(sql);
            return new Promise(function (resolve, reject) {
                getConnetion(sql, function (err, result, fields) {
                    if (err) {
                        reject(err);
                    } else {
                        if (result && result.length > -1) {
                            resolve({ status: 1, msg: text + "成功", result: result });
                        } else {
                            resolve({ status: 0, msg: text + '失败'});
                        }
                    }
                })
            })
        },

        selectByWhere: function (tableName, where, text) {
            console.log('-----------------------');
            console.log(where);
            let sql = `select * from ${tableName} where ${where}`;
            console.log(sql);
            return new Promise(function (resolve, reject) {
                getConnetion(sql, function (err, result, fields) {
                    if (err) {
                        reject(err);
                    } else {
                        if (result && result.length) {
                            resolve({ status: 1, result: result });
                        } else {
                            resolve({ status: 0, msg: text + ' 失败' });
                        }
                    }
                })
            })
        },

        selectAll: function (tableName) {
            let sql = `select * from ${tableName}`;
            console.log('-----------------------');
            console.log(sql);
            return new Promise(function (resolve, reject) {
                getConnetion(sql, function (err, result, fields) {
                    if (err) {
                        reject(err);
                    } else {
                        if (result && result.length) {
                            resolve({ status: 1, result: result });
                        } else {
                            resolve({ status: 0, msg: ' 没有查询到任何数据' });
                        }
                    }
                })
            })
        },

        updateById: function (tableName, args, text) {
            delete tableName.userid;
            let sql = `update ${tableName} set ${args.keys.join(",")} where ${args.where}`;
            console.log('-----------------------');
            console.log(sql);
            return new Promise(function (resolve, reject) {
                getConnetion(sql, function (err, result, fields) {
                    if (err) {
                        reject(err);
                    } else {
                        if (result && result.length) {
                            resolve({ status: 1, msg: text + ' 成功' });
                        } else {
                            resolve({ status: 0, msg: text + ' 失败' });
                        }
                    }
                })
            })
        },

        delete: function (tableName, where, text) {
            let sql = `delete from ${tableName} where ${where}`;
            console.log('-----------------------');
            console.log(sql);
            return new Promise(function (resolve, reject) {
                getConnetion(sql, function (err, result, fields) {
                    if (err) {
                        reject(err);
                    } else {
                        if (result && result.affectedRows) {
                            resolve({ status: 1, msg: text + ' 成功' });
                        } else {
                            resolve({ status: 0, msg: text + ' 失败' });
                        }
                    }
                })
            })
        }
    }
}

module.exports = mysql_help;
