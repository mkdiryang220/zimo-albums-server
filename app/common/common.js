import dbConstructs from "../config/dbConstructs.js"
import _ from "lodash"

// 公共方法
function assignSqlArg(keys, obj){
  let result = []
  keys.forEach((key, index) => {
    if (obj[key]){
      result.push(`${key}='${obj[key]}'`)
    }
  });
  return result
}

// 调整参数顺序
function sortArg(name, data){
  let result = [];
  let ArgEnum = _.keys(dbConstructs[name]);
  let ArgData = _.keys(data);
  if (ArgEnum.length === ArgData.length){
    ArgEnum.forEach(function (key) {
      result.push(data[key]);
    })
    return result
  }else{
    return "common.js:sortArg_枚举参数和传入参数字段不相等"
  }

}

// 转换对象为where条件语句
function whereField(field, orAnd){
  let condition = orAnd==='or' ? ' or ' : ' and '
  let keys = _.keys(field);
  let result = [];
  keys.forEach(function(key){
    result.push(`${key}='${field[key]}'`);
  })
  return result.join(condition)
}

module.exports = {
  assignSqlArg: assignSqlArg,
  sortArg: sortArg,
  whereField: whereField
}