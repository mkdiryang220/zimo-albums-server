import mysqlhelp from './mysql.help'
import uuidV1 from 'uuid/v1'
import common from './common'
import _ from 'lodash'
import dbConstructs from '../config/dbConstructs'
const SearchJobs = mysqlhelp('jobs');

// node 内部通用接口
function useDBAsApi(name) {
  let table_name = name;
  let dbConstruct = dbConstructs[name]
  let row_id_name = Object.keys(dbConstruct)[0]
  let userText = getTextTip(name)

  if (!table_name) {
    console.error("缺少表名")
    return
  } else if (!row_id_name) {
    console.error("缺少行 id")
    return
  } else if (!userText) {
    console.error("缺少提示字符")
    return
  }
  return {
    getAllRows: function () {
      return SearchJobs.selectAll(table_name)
    },
    getRowById: function (id, name) {   // 查询一个id, id为字符串
      let field_name = name ? name : row_id_name
      let where = `${field_name}='${id}'`;
      return SearchJobs.selectByWhere(table_name, where, userText.find)
    },
    getRowByIds: function (ids, name) { // 查询多个id或者其他字段, ids为数组
      let field_name = name ? name : row_id_name
      let idsField = [];
      ids.forEach(id => {
        idsField.push(`'${id}'`)
      });
      let where = `${field_name} in (${idsField.join(',')})`
      return SearchJobs.selectByWhere(table_name, where, userText.find)
    },
    getRowByWhere: function (field, orAnd) {
      let where = common.whereField(field, orAnd);
      return SearchJobs.selectByWhere(table_name, where, userText.find)
    },
    addRow: function (rowData) {
      rowData[row_id_name] = uuidV1()
      let arg = common.sortArg(name, rowData);
      return SearchJobs.insert(table_name, arg, userText.create);
    },
    updataRow: function () {
      delete dbConstruct[row_id_name]
      let arg = {
        keys: common.assignSqlArg(_.keys(dbConstruct), req.body),
        where: `${row_id_name}='${req.body[row_id_name]}'`
      }
      return SearchJobs.updateById(table_name, arg, userText.update)
    },
    deleteRow: function (id, name) {
      let field_name = name ? name : row_id_name
      let where = `${field_name} in('${id}')`;
      return SearchJobs.delete(table_name, where, userText.delete)
    }
  }
}

function getTextTip(name) {
  let tipText = {
    user: "用户信息",
    role: "角色信息",
    resource: "路由信息",
    user_role: "用户角色关系表",
    role_resource: "角色路由关系表",
    photo: "照片信息",
    album: "相册信息",
    video: "视频信息",
    album_photo: "相册照片关联信息",
    intercepting: "",
  }
  let text = tipText[name]
  return {
    create: "添加" + text,
    update: "更新" + text,
    delete: "删除" + text,
    find: "查询" + text
  }
}

module.exports = {
  useDBAsApi
}








// node 路由通用接口
// function searchDBAsRouter(name){
//   let table_name = table_enum[name].table_name
//   let row_id_name = table_enum[name].row_id_name
//   let userText = table_enum[name].userText
//   let dbConstruct = dbConstructs[name]
//   if (!table_name){
//     console.error("缺少表名")
//     return
//   } else if (!row_id_name){
//     console.error("缺少行 id")
//     return
//   } else if (!userText) {
//     console.error("缺少提示字符")
//     return
//   }
//   return {
//     getAllRows: function (req, res, next) {
//       SearchJobs.selectAll(table_name).then(function (data) {
//         res.json(data)
//       })
//     },
//     getRowById: function (req, res, next) {
//       let where = `${row_id_name}='${req.body[row_id_name]}'`;
//       SearchJobs.selectById(table_name, where, userText.find).then(function (data) {
//         res.json(data)
//       })
//     },
//     addRow: function (req, res, next) {
//       let arg = _.values(req.body)
//       arg.unshift(uuidV1())

//       SearchJobs.insert(table_name, arg, userText.create).then(function (data) {
//         res.json(data)
//       })
//     },
//     updataRow: function (req, res, next) {
//       delete dbConstruct[row_id_name]
//       let arg = {
//         keys: common.assignSqlArg(_.keys(dbConstruct), req.body),
//         where: `${row_id_name}='${req.body[row_id_name]}'`
//       }
//       SearchJobs.updateById(table_name, arg, userText.update).then(function (data) {
//         res.json(data)
//       })
//     },
//     deleteRow: function (req, res, next) {
//       let where = `${row_id_name} in('${req.body.userid}')`;
//       SearchJobs.delete(table_name, where, userText.delete).then(function (data) {
//         res.json(data)
//       })
//     }
//   }
// }

// 创建通用路由
// function createUtilRouters(app, name) {
//   const mysqlUtilApi = searchDBAsRouter(name)
//   app.get('/' + name, mysqlUtilApi.getAllRows)
//   app.get('/' + name + 'getAllRows', mysqlUtilApi.getAllRows)
//   app.post('/' + name + '/getRowById', mysqlUtilApi.getRowById)
//   app.post('/' + name + '/addRow', mysqlUtilApi.addRow)
//   app.post('/' + name + '/updataRow', mysqlUtilApi.updataRow)
//   app.post('/' + name + '/deleteRow', mysqlUtilApi.deleteRow)
// }
// const name = "album"
// import MysqlUtil from '../common/mysql.util'

// module.exports = function (app) {
//   MysqlUtil.createUtilRouters(app, name)
// }
  // createUtilRouters,