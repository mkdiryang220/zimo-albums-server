let mysql = require('mysql');
let msconfig = require('../config/config').mysql;
let logger = require('./util/logger')();
let dbs = ['jobs'];
let pools = {};

dbs.forEach(function (db) {
  pools[db] = mysql.createPool({
    host: msconfig.host,
    user: msconfig.user,
    password: msconfig.password,
    database: db
  })
});

function sqlOption(db) {
  let pool = pools[db];
  let getConnetion = function (sql, cb) {
    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error(err);
        throw err
      }
      connection.query(sql, function (error, results, fields) {
        if (error) {
          logger.error(error);
        }
        cb(error, results, fields);
        connection.release();
      })
    })
  };
  let debug = function(sql){
    if (process.env && process.env.DEBUG == 'dev'){
      console.log('-----------------------')
      console.log(sql)
    }
  }
  return {
    insert: function (tableName, values, text) {
      let newVal = []
      values.forEach(function(value){
        newVal.push('"'+ value + '"')
      })
      let v = newVal.join(",");
      let sql = `INSERT INTO ${tableName} VALUES (${v})`;

      debug(sql)

      return new Promise(function (resolve, reject) {
        getConnetion(sql, function (err, result, fields) {
          if (err) {
            reject(err);
          } else {
            if (result && result.insertId > -1) {
              resolve({ status: 1, msg: text + "成功", result: values });
            } else {
              resolve({ status: 0, msg: text + '失败'});
            }
          }
        })
      })
    },

    selectByWhere: function (tableName, where, text) {
      let sql = `select * from ${tableName} where ${where}`;
      debug(sql)

      return new Promise(function (resolve, reject) {
        getConnetion(sql, function (err, result, fields) {
          if (err) {
            reject(err);
          } else {
            if (result && result.length) {
              resolve({ status: 1, result: result });
            } else {
              resolve({ status: 0, msg: text + '失败' });
            }
          }
        })
      })
    },

    selectAll: function (tableName) {
      let sql = `select * from ${tableName}`;
      debug(sql)

      return new Promise(function (resolve, reject) {
        getConnetion(sql, function (err, result, fields) {
          if (err) {
            reject(err);
          } else {
            if (result && result.length) {
              resolve({ status: 1, result: result });
            } else {
              resolve({ status: 0, msg: '没有查询到任何数据' });
            }
          }
        })
      })
    },

    updateById: function (tableName, args, text) {
      delete tableName.userid
      let sql = `update ${tableName} set ${args.keys.join(",")} where ${args.where}`
      debug(sql)

      return new Promise(function (resolve, reject) {
        getConnetion(sql, function (err, result, fields) {
          if (err) {
            reject(err);
          } else {
            if (result && result.affectedRows > 0) {
              resolve({ status: 1, msg: text + '成功' });
            } else {
              resolve({ status: 0, msg: text + '失败' });
            }
          }
        })
      })
    },

    delete: function (tableName, where, text) {
      let sql = `delete from ${tableName} where ${where}`
      debug(sql)

      return new Promise(function (resolve, reject) {
        getConnetion(sql, function (err, result, fields) {
          if (err) {
            reject(err);
          } else {
            if (result && result.affectedRows > 0) {
              resolve({ status: 1, msg: text + '成功' });
            } else {
              resolve({ status: 0, msg: text + '失败' });
            }
          }
        })
      })
    }
  }
}

module.exports = sqlOption;
