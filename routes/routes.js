import InterServer from '../app/server/interServer'
import PhotoServer from '../app/server/photoServer'
import VideoServer from '../app/server/videoServer'
import LoginServer from '../app/server/loginServer'
import AlbumServer from '../app/server/albumServer'

module.exports = function(app){
  InterServer(app)
  LoginServer(app)
  PhotoServer(app)
  VideoServer(app)
  AlbumServer(app)
}