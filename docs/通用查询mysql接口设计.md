## 通用接口设计

给项目配置 [数据库连接参数(app/config/config.js)、数据库字段(app/config/dbConstructs.js)、查询参数(app/config/table_enum.js)]

在 app/server/ 下创建 user.js, 写入以下代码

```
const name = "user"
import MysqlUtil from '../common/mysql.util'

module.exports = function (app) {
  MysqlUtil.createUtilRouters(app, name);
}
```

和在 app/server/ 下创建 role.js, 写入以下代码

```
const name = "role"
import MysqlUtil from '../common/mysql.util'

module.exports = function (app) {
  MysqlUtil.createUtilRouters(app, name);
}

```

这样 user 和 role 表都存在以下通用查询 mysql 的接口

```
getAllRows 查询全表
getRowById 通过 id 查询一行数据
addRow     加入一行数据
updataRow  更新一行数据
deleteRow  删除一行数据
```

由于直接使用路由方式直接获取数据库数据可能不是我们想要的  

所以大部分时候以上代码只能当作开发测试使用, 如果需要根据业务需求混合数据,  

调用 mysql.util.js 内的 searchDBAsApi(name)  

如：

```
    let userAllRows = MysqlUtil.searchDBAsApi('user').getAllRows()
    let roleAllRows = MysqlUtil.searchDBAsApi('role').getAllRows()
    let userRoleAllRows = MysqlUtil.searchDBAsApi('user_role').getAllRows()
    Promise.all([userAllRows, roleAllRows, userRoleAllRows ]).then(function(result){
      let userAllDate = result[0] // 得到所有用户数据
      let roleAllDate = result[1] // 得到所有用户角色
      let userRoleAllDate = result[2] // 得到所有用户角色关系表
      // 混合数据返回结果
    })
```

使用以上方式混合数据生成所需 API。






