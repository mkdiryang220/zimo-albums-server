POST user/register 注册
POST user/login    登陆
POST uploadPhotos 上传照片

```
1.上传照片到七牛云存储
2.拿到返回信息保存照片数据到照片表。
3.保存照片相册 id 到关系表。
```

POST moveToAlbum 移动照片到指定相册

```
1.更新关系表
```

POST deletePhoto 删除照片

```
1.删除关系表相关照片信息
2.删除照片表照片信息
```

POST addAlbum 新增相册
```
1.调用相册接口
```

GET getUserInfo 获取用户相关所有信息
